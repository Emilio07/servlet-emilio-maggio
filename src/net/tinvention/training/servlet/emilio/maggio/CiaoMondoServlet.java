package net.tinvention.training.servlet.emilio.maggio;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class CiaoMondoServlet extends HttpServlet {
	synchronized protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		out.println("<HTML><HEAD><TITLE>SalveMondo!</TITLE>");
		out.println("</HEAD><BODY>SalveMondo!</BODY>");
		out.println("<br/><a href='servletA'>reload</a>");
		HttpSession session = req.getSession(true);
		int numberBr = 1;
		if (session.getAttribute("numberCallsBrowser") == null) {
			session.setAttribute("numberCallsBrowser", 1);
		} else {
			numberBr = (int) session.getAttribute("numberCallsBrowser") + 1;
			session.setAttribute("numberCallsBrowser", numberBr);
		}
		out.println("<br/> number of calls of servlet in browser</HTML>:" + numberBr);
		ServletContext application = this.getServletContext();
		int numberAp = 1;
		if (application.getAttribute("numberCallsWebapp") == null) {
			application.setAttribute("numberCallsWebapp", 1);
		} else {
			numberAp = (int) application.getAttribute("numberCallsWebapp") + 1;
			application.setAttribute("numberCallsWebapp", numberAp);
		}
		out.println("<br/> number of calls of servlet in webapp</HTML>:" + numberAp);
		out.close();
	}

	synchronized protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		if ((Integer.parseInt(req.getParameter("hours")) >= 20) | (Integer.parseInt(req.getParameter("hours")) < 6)) {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/servletB/*");
			dispatcher.forward(req, res);
		} else {
			res.sendRedirect("servletA");

		}
	}
}
