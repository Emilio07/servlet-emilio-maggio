package net.tinvention.training.servlet.emilio.maggio;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
@SuppressWarnings("serial")
public class BuonaSeraServlet extends HttpServlet{
	synchronized protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException{
			res.setContentType("text/html");
			PrintWriter out =res.getWriter();
			out.println("<HTML><HEAD><TITLE>BuonaSera!</TITLE>");
            out.println("</HEAD><BODY>Buona Sera</BODY>");
			out.close();
}
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.sendRedirect("servletB");
	}
}
